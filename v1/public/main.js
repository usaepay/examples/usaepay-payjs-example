document.addEventListener('DOMContentLoaded', function () {
	document.getElementById('pickPaymentCard').addEventListener('click', event => {
		window.location.href = '/card'
	});
	document.getElementById('pickApplePay').addEventListener('click', event => {
		window.location.href = '/applepay'
	});
});