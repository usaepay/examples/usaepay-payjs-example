const express = require("express");
const morgan = require('morgan');
const https = require('https');
const fs = require('fs');
const app = express();
if (process.env.NODE_ENV !== 'production') {
	require('dotenv').config();
}

const sectionFileList = ['card', 'applepay'];
// set public key in JS files from .env
sectionFileList.forEach(section => {
	const data = fs.readFileSync(__dirname + `/public/${section}/${section}.js`, 'utf-8');
	const regex = new RegExp('var publicKey.*;');
	const newValue = data.replace(regex, `var publicKey = '${process.env.PUBLIC_KEY}';`);
	fs.writeFileSync(__dirname + `/public/${section}/${section}.js`, newValue, 'utf-8');
});
// set host in HTML files from .env
sectionFileList.forEach(section => {
	const data = fs.readFileSync(__dirname + `/public/${section}/${section}.html`, 'utf-8');
	const regex = new RegExp('src=".*/js/v');
	const newValue = data.replace(regex, `src="https://${process.env.HOST_NAME}/js/v`);
	fs.writeFileSync(__dirname + `/public/${section}/${section}.html`, newValue, 'utf-8');
});

app.use(express.static("public", {
	index: [
		'index.html',
		'card.html',
		'applepay.html'
	] 
}));
app.use(require('body-parser').json());
app.use(morgan('common'));

// domain validaion endpoint for apple pay
app.get('/.well-known/apple-developer-merchantid-domain-association.txt', (req, res) => {
	res.sendFile(__dirname + '/public/apple-developer-merchantid-domain-association.txt');
})

// send request to transactions api
app.post('/charge', (req, res) => {
	var requestBody = JSON.stringify(req.body);;
	var sha256 = require('sha256');
	var seed = Math.random() * 8675309;
	var prehash = process.env.API_KEY + seed + process.env.API_PIN;
	var apihash = 's2/' + seed + '/' + sha256(prehash);
	var authKey = new Buffer.from(process.env.API_KEY + ":" + apihash).toString('base64')
	var requestOptions = {
		method: 'POST',
		hostname: process.env.HOST_NAME,
		path: '/api/v2/transactions',
		headers: {
			'Authorization': 'Basic ' + authKey,
			'Content-Type': 'application/json',
			'Content-Length': requestBody.length
		},
		rejectUnauthorized: false,
		requestCert: true,
		agent: false
	}
	var transResBody = '';
	var transReq = https.request(requestOptions, transRes => {
		transRes.on('data', (d) => {
			transResBody = JSON.parse(d.toString());
		});
		transRes.on('end', () => {
			res.status(200).json(transResBody);
			res.end('ok');
		});
	});

	transReq.on('error', (e) => {
		console.error(e);
		res.status(500).json({error: 'Unexpected Server Error'});
	});

	transReq.write(requestBody);
	transReq.end();
});

app.listen(process.env.PORT || 8080, () => {
	console.log(`Your app is listening on port ${process.env.PORT || 8080}`);
});