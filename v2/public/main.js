document.addEventListener('DOMContentLoaded', function () {
	document.getElementById('pickPaymentCard').addEventListener('click', event => {
		window.location.href = '/card'
	});
	document.getElementById('pickPaymentCheck').addEventListener('click', event => {
		window.location.href = '/check'
	});
	document.getElementById('pickApplePay').addEventListener('click', event => {
		window.location.href = '/applepay'
	});
});