var client, applePay;
var publicKey = '_W111HAO0u040urk6mc42byiyj2P3hMS1kbXrygxS5';

var applePayConfig = {
	targetDiv: 'applePayContainer',
	displayName: 'Test Company',
	applePayBtn: {
		type: 'buy',
		color: 'white-outline'
	},
	paymentRequest: {
		lineItems: [
			{
				label: 'subtotal',
				amount: '0.91',
				type: 'final'
			},
			{
				label: 'tax',
				amount: '0.09',
				type: 'final'
			}
		],
		total: {
			label: 'Test Company',
			amount: '1.00',
			type: 'final'
		},
		countryCode: 'US',
		currencyCode: 'USD'
	}
}
var transactionInfo = {
	command: 'sale',
	payment_key: '',
	amount: '1.00',
	amount_detail: { tax: '0.09', subtotal: '0.91' },
	custemailaddr: 'test@noreply.com',
	billing_address: {
		firstname: 'Testy',
		lastname: 'McTesterson',
		street: '123 Main St',
		street2: '',
		cstreet2ity: 'Test City',
		state: 'TX',
		postalcode: '12345',
		country: 'United States',
		phone: '18008888888'
	}
}

document.addEventListener('DOMContentLoaded', function () {
	startApplePayDemo();
});

function startApplePayDemo() {
	initializeApplePayEntry();

	const applePayConfigEl = document.getElementById('applePayConfig');
	applePayConfigEl.value = JSON.stringify(applePayConfig, null, 4);

	// click listeners
	document.getElementById('updateConfigBtn').addEventListener('click', event => {
		refreshPaymentEl();
	});
	document.getElementById('submitBtn').addEventListener('click', event => {
		submitTransaction();
	})
	document.getElementById('backBtn').addEventListener('click', event => {
		window.location.href = '/';
	});

}

function initializeApplePayEntry() {
	// instantiate client with public_key
	client = new usaepay.Client(publicKey);

	// instantiate Payment Card Entry
	applePay = client.createApplePayEntry(applePayConfig);
	
	// check to see if Apple Pay is available
	applePay.checkCompatibility().then(res => {
		// if so, add the Apple Pay button and unhide config ection
		document.getElementById('configContainer').classList.remove('hidden');
		applePay.addButton();
	}).catch(err => {
		// if not, hide the section it would have gone into
		document.getElementById('applePayContainer').style.display = 'none';
		console.error(err);
		handleError(err, 'applePay');
	})

	// listen for Apple Pay to be successfully completed
	applePay.on('applePaySuccess', function () {
		clearError('applePay');
		// once successfully completed, payment key is ready
		client.getPaymentKey(applePay).then(result => {
			paymentKeyHandler(result);
			const transInfoInputEl = document.getElementById('transactionInfo');
			transactionInfo.payment_key = result;
			transInfoInputEl.value = JSON.stringify(transactionInfo, null, 4);
			document.getElementById('transactionInfoContainer').classList.remove('hidden');
		}).catch(err => {
			console.error(err);
			handleError(err, 'applePay');
		});
	});

	// listen for any Apple Pay errors
	applePay.on('applePayError', function () {
		handleError('applePayError', 'applePay');
	})
}

function refreshPaymentEl() {
	clearError('config');
	const applePayConfigVal = document.getElementById('applePayConfig').value;
	try {
		startLoading('updateConfigBtn');
		applePayConfig = JSON.parse(applePayConfigVal);
		initializeApplePayEntry();
		stopLoading('updateConfigBtn');
	} catch (err) {
		console.error(err);
		stopLoading('updateConfigBtn');
		handleError('Invalid JSON', 'config');
	}
	
}

function submitTransaction() {
	clearError('transaction');
	startLoading('submitBtn');
	const transResEl = document.getElementById('transactionResult');
	const transInfoInputEl = document.getElementById('transactionInfo');
	fetch('/charge', {
		method: 'post',
		headers: {
			'Accept': 'application/json, text/plain, */*',
			'Content-Type': 'application/json'
		},
		body: transInfoInputEl.value
	}).then(res => {
		stopLoading('submitBtn');
		if (res.ok) {
			return res.json();
		} else {
			console.error(res);
			throw Error(res.statusText);
		}
	}).then(res => {
		document.getElementById('transactionResultContainer').classList.remove('hidden');
		transResEl.value = JSON.stringify(res, null, 4);
	}).catch(err => {
		handleError(err, 'transaction');
	});
}

function startLoading(id) {
	const btn = document.getElementById(id);
	btn.setAttribute('disabled', true);
	const btnIcon = btn.getElementsByTagName('i')[0];
	btnIcon.classList.remove('far', 'fa-arrow-alt-circle-right');
	btnIcon.classList.add('fas', 'fa-circle-notch', 'fa-spin');
}

function stopLoading(id) {
	const btn = document.getElementById(id);
	btn.removeAttribute('disabled');
	const btnIcon = btn.getElementsByTagName('i')[0];
	btnIcon.classList.remove('fas', 'fa-circle-notch', 'fa-spin');
	btnIcon.classList.add('far', 'fa-arrow-alt-circle-right');
}

function handleError(errorMessage, field) {
	var errorContainerEl = document.getElementById(`${field}ErrorContainer`);
	errorContainerEl.textContent = errorMessage;
	errorContainerEl.classList.remove('hidden');
}

function clearError(field) {
	var errorContainerEl = document.getElementById(`${field}ErrorContainer`);
	errorContainerEl.textContent = '';
	errorContainerEl.classList.add('hidden');
}
