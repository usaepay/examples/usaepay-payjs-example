var client, paymentCheck;
var publicKey = '_W111HAO0u040urk6mc42byiyj2P3hMS1kbXrygxS5';

var payCheckConfig = {
	styles: {
		'base': {
			'font-size': '1rem'
		}
	},
	placeholders: {
		'routing': 'Routing No.',
		'account': 'Account No.',
		'account-verify': 'Verify Account No.',
		'dl-num': 'Driver\'s License No.',
		'dl-state': 'DL State'

	},
	dl_required: false
};
var transactionInfo = {
	command: 'sale',
	payment_key: '',
	amount: '5.00',
	amount_detail: { tax: '0.50', subtotal: '4.50' },
	custemailaddr: 'goku@capsulecorp.com',
	billing_address: {
		firstname: 'Son',
		lastname: 'Goku',
		street: '123 Main St',
		street2: '',
		cstreet2ity: 'West City',
		state: 'CA',
		postalcode: '98765',
		country: 'United States',
		phone: '18008888888'
	}
}

document.addEventListener('DOMContentLoaded', function () {
	startPaymentCheckDemo();
});

function startPaymentCheckDemo() {
	initializePaymentCheckEntry();

	const styleInputEl = document.getElementById('payCheckConfig');
	styleInputEl.value = JSON.stringify(payCheckConfig, null, 4);

	// click listeners
	document.getElementById('updateConfigBtn').addEventListener('click', () => {
		refreshPaymentEl();
	});
	document.getElementById('getPaymentKeyBtn').addEventListener('click', () => {
		getPaymentKey();
	});
	document.getElementById('submitBtn').addEventListener('click', () => {
		submitTransaction();
	});
	document.getElementById('backBtn').addEventListener('click', () => {
		window.location.href = '/';
	});

}

function initializePaymentCheckEntry() {
	// instantiate client with public_key
	client = new usaepay.Client(publicKey);

	// instantiate Payment Check Entry
	paymentCheck = client.createPaymentCheckEntry();
	paymentCheck.generateHTML(payCheckConfig);

	// use the id of the div where you want the credit check field inserted
	paymentCheck.addHTML('paymentCheckContainer');

	// listen for errors so that you can display error messages
	paymentCheck.addEventListener('error', errorMessage => {
		if (errorMessage.length === 0) {
			clearError('paymentCheck');
		} else {
			handleError(errorMessage, 'paymentCheck');
		}
	});
}

function refreshPaymentEl() {
	clearError('payCheckConfig');
	const styleInput = document.getElementById('payCheckConfig').value;
	try {
		startLoading('updateConfigBtn');
		payCheckConfig = JSON.parse(styleInput);
		initializePaymentCheckEntry();
		stopLoading('updateConfigBtn');
	} catch (err) {
		stopLoading('updateConfigBtn');
		handleError('Invalid JSON', 'payCheckConfig');
	}
	
}

function getPaymentKey() {
	clearError('paymentCheck');
	startLoading('getPaymentKeyBtn');
	// create a payment key, returns a promise which will resolve in an error or the payment key
	client.getPaymentKey(paymentCheck).then(result => {
		const paymentKeyResEl = document.getElementById('paymentKeyRes');
		paymentKeyResEl.value = JSON.stringify(result, null, 4);
		transactionInfo.payment_key = result.key;
		const transInfoInputEl = document.getElementById('transactionInfo');
		transInfoInputEl.value = JSON.stringify(transactionInfo, null, 4);
		document.getElementById('transactionInfoContainer').classList.remove('hidden');
		stopLoading('getPaymentKeyBtn');
	}).catch(errorMessage => {
		handleError(errorMessage, 'paymentCheck');
		stopLoading('getPaymentKeyBtn');
	});
}

function submitTransaction() {
	clearError('transaction');
	startLoading('submitBtn');
	const transResEl = document.getElementById('transactionResult');
	const transInfoInputEl = document.getElementById('transactionInfo');
	fetch('/charge', {
		method: 'post',
		headers: {
			'Accept': 'application/json, text/plain, */*',
			'Content-Type': 'application/json'
		},
		body: transInfoInputEl.value
	}).then(res => {
		stopLoading('submitBtn');
		if (res.ok) {
			return res.json();
		} else {
			console.error(res);
			throw Error(res.statusText);
		}
	}).then(res => {
		document.getElementById('transactionResultContainer').classList.remove('hidden');
		transResEl.value = JSON.stringify(res, null, 4);
	}).catch(err => {
		handleError(err, 'transaction');
	});
}

function startLoading(id) {
	const btn = document.getElementById(id);
	btn.setAttribute('disabled', true);
	const btnIcon = btn.getElementsByTagName('i')[0];
	btnIcon.classList.remove('far', 'fa-arrow-alt-circle-right');
	btnIcon.classList.add('fas', 'fa-circle-notch', 'fa-spin');
}

function stopLoading(id) {
	const btn = document.getElementById(id);
	btn.removeAttribute('disabled');
	const btnIcon = btn.getElementsByTagName('i')[0];
	btnIcon.classList.remove('fas', 'fa-circle-notch', 'fa-spin');
	btnIcon.classList.add('far', 'fa-arrow-alt-circle-right');
}

function handleError(errorMessage, field) {
	var errorContainerEl = document.getElementById(`${field}ErrorContainer`);
	errorContainerEl.textContent = errorMessage;
	errorContainerEl.classList.remove('hidden');
}

function clearError(field) {
	var errorContainerEl = document.getElementById(`${field}ErrorContainer`);
	errorContainerEl.textContent = '';
	errorContainerEl.classList.add('hidden');
}
