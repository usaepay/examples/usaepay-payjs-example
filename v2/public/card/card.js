var client, paymentCard;
var publicKey = '_W111HAO0u040urk6mc42byiyj2P3hMS1kbXrygxS5';

var payCardConfig = {
	styles: {
		'base': {
			'font-size': '1rem',
			'padding': '2px 0'
		}
	},
	placeholders: {
		cnum: 'Card Number',
		exp: 'MM/YY',
		cvv: 'CVV'
	},
	cvv_required: true
};
var transactionInfo = {
	command: 'sale',
	payment_key: '',
	amount: '5.00',
	amount_detail: { tax: '0.50', subtotal: '4.50' },
	custemailaddr: 'goku@capsulecorp.com',
	billing_address: {
		firstname: 'Son',
		lastname: 'Goku',
		street: '123 Main St',
		street2: '',
		cstreet2ity: 'West City',
		state: 'CA',
		postalcode: '98765',
		country: 'United States',
		phone: '18008888888'
	}
}

document.addEventListener('DOMContentLoaded', function () {
	startPaymentCardDemo();
});

function startPaymentCardDemo() {
	initializePaymentCardEntry();

	const styleInputEl = document.getElementById('payCardConfig');
	styleInputEl.value = JSON.stringify(payCardConfig, null, 4);

	// click listeners
	document.getElementById('updateConfigBtn').addEventListener('click', () => {
		refreshPaymentEl();
	});
	document.getElementById('getPaymentKeyBtn').addEventListener('click', () => {
		getPaymentKey();
	});
	document.getElementById('submitBtn').addEventListener('click', () => {
		submitTransaction();
	});
	document.getElementById('backBtn').addEventListener('click', () => {
		window.location.href = '/';
	});

}

function initializePaymentCardEntry() {
	// instantiate client with public_key
	client = new usaepay.Client(publicKey);

	// instantiate Payment Card Entry
	paymentCard = client.createPaymentCardEntry();
	paymentCard.generateHTML(payCardConfig);

	// use the id of the div where you want the credit card field inserted
	paymentCard.addHTML('paymentCardContainer');

	// listen for errors so that you can display error messages
	paymentCard.addEventListener('error', errorMessage => {
		if (errorMessage.length === 0) {
			clearError('paymentCard');
		} else {
			handleError(errorMessage, 'paymentCard');
		}
	});
}

function refreshPaymentEl() {
	clearError('payCardConfig');
	const styleInput = document.getElementById('payCardConfig').value;
	try {
		startLoading('updateConfigBtn');
		payCardConfig = JSON.parse(styleInput);
		initializePaymentCardEntry();
		stopLoading('updateConfigBtn');
	} catch (err) {
		stopLoading('updateConfigBtn');
		handleError('Invalid JSON', 'payCardConfig');
	}
	
}

function getPaymentKey() {
	clearError('paymentCard');
	startLoading('getPaymentKeyBtn');
	// create a payment key, returns a promise wchich will resolve in an error or the payment key
	client.getPaymentKey(paymentCard).then(result => {
		const paymentKeyResEl = document.getElementById('paymentKeyRes');
		paymentKeyResEl.value = JSON.stringify(result, null, 4);
		transactionInfo.payment_key = result.key;
		const transInfoInputEl = document.getElementById('transactionInfo');
		transInfoInputEl.value = JSON.stringify(transactionInfo, null, 4);
		document.getElementById('transactionInfoContainer').classList.remove('hidden');
		stopLoading('getPaymentKeyBtn');
	}).catch(errorMessage => {
		handleError(errorMessage, 'paymentCard');
		stopLoading('getPaymentKeyBtn');
	});
}

function submitTransaction() {
	clearError('transaction');
	startLoading('submitBtn');
	const transResEl = document.getElementById('transactionResult');
	const transInfoInputEl = document.getElementById('transactionInfo');
	fetch('/charge', {
		method: 'post',
		headers: {
			'Accept': 'application/json, text/plain, */*',
			'Content-Type': 'application/json'
		},
		body: transInfoInputEl.value
	}).then(res => {
		stopLoading('submitBtn');
		if (res.ok) {
			return res.json();
		} else {
			console.error(res);
			throw Error(res.statusText);
		}
	}).then(res => {
		document.getElementById('transactionResultContainer').classList.remove('hidden');
		transResEl.value = JSON.stringify(res, null, 4);
	}).catch(err => {
		handleError(err, 'transaction');
	});
}

function startLoading(id) {
	const btn = document.getElementById(id);
	btn.setAttribute('disabled', true);
	const btnIcon = btn.getElementsByTagName('i')[0];
	btnIcon.classList.remove('far', 'fa-arrow-alt-circle-right');
	btnIcon.classList.add('fas', 'fa-circle-notch', 'fa-spin');
}

function stopLoading(id) {
	const btn = document.getElementById(id);
	btn.removeAttribute('disabled');
	const btnIcon = btn.getElementsByTagName('i')[0];
	btnIcon.classList.remove('fas', 'fa-circle-notch', 'fa-spin');
	btnIcon.classList.add('far', 'fa-arrow-alt-circle-right');
}

function handleError(errorMessage, field) {
	var errorContainerEl = document.getElementById(`${field}ErrorContainer`);
	errorContainerEl.textContent = errorMessage;
	errorContainerEl.classList.remove('hidden');
}

function clearError(field) {
	var errorContainerEl = document.getElementById(`${field}ErrorContainer`);
	errorContainerEl.textContent = '';
	errorContainerEl.classList.add('hidden');
}
