# Get Started

1. clone the repo

2. create a file called `.env` in the root of the repo and, using `.env-example` as a guide, set the needed variables

3. open a terminal window and `cd` into the directory of whichever version of pay.js you'd like to demo (ex. `cd <path to cloned repo>/usaepay-payjs-example/v1`)

4. run `npm install`

5. run `node app.js`

6. go to localhost:8080 in your browser (or whatever port you designate)

For Apple Pay to work you'll need to be on Safari and use ngrok or something similar to get a secure public URL for your localhost. See ngrok docs here: https://ngrok.com/docs. On the free tier you only get random URLs so you'll have to re-validate your url with Apple every time you restart ngrok. For a quick start: 

1. Open a new terminal window or tab

2. run `npm install -g ngrok`

3. run `ngrok http 8080`

4. navigate in your browser to the https address that gets printed in the terminal after `Forwarding`

*Note: If you're setting up Apple Pay certificates using this app: replace the contents of the `apple-developer-merchantid-domain-association.txt` file in the public folder with your own value when validating your domain*

# Instructions

1. On page load you'll see style/config text area that you can edit and the corresponding payemnt element. After making any changes hit the `Apply Changes` button to regenerate the payment element with the new config/styles. You can also change the values in the corresponding .js file to make it permanent.

2. The next step is to get a payment key, either click the payment button and follow all of the prompts or enter payment info and click the `Get Payment Key` button.

3. If successful the Transaction Info textarea will appear with the payment key already inside. This info can also be edited as desired. 

4. To send the transaction click the `Submit Payment Form` button. The results will appear in a new `Transaction Results` textarea.